package com.johnny.princestheatre.di

import android.content.Context
import androidx.room.Room
import com.johnny.princestheatre.database.AppDatabase
import com.johnny.princestheatre.database.DBTypeConverters
import com.johnny.princestheatre.database.dao.MediaDao
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Named

@Module
@InstallIn(ApplicationComponent::class)
object TestDatabaseModule {
    @Provides
    @Named("test_db")
    fun provideInMemoryDatabase(@ApplicationContext context: Context, moshi: Moshi): AppDatabase {
        DBTypeConverters.initialise(moshi)
        return Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java)
            .allowMainThreadQueries()
            .build()
    }
}
