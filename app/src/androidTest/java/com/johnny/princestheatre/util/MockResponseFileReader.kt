package com.johnny.princestheatre.util

import java.io.InputStreamReader

/*
    From https://gist.github.com/fievx/60bd1b79f807d866eb12e1b06cd1d7c0
 */
class MockResponseFileReader(path: String) {
    val content: String

    init {
        val reader = InputStreamReader(this.javaClass.classLoader!!.getResourceAsStream(path))
        content = reader.readText()
        reader.close()
    }
}
