package com.johnny.princestheatre.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import com.johnny.princestheatre.database.AppDatabase
import com.johnny.princestheatre.database.dao.DetailDao
import com.johnny.princestheatre.database.dao.ProviderDao
import com.johnny.princestheatre.database.entity.*
import com.johnny.princestheatre.di.TestNetworkModule.MOCK_WEB_SERVER_PORT
import com.johnny.princestheatre.model.MediaDetail
import com.johnny.princestheatre.network.api.CinemaWorldApi
import com.johnny.princestheatre.network.api.FilmWorldApi
import com.johnny.princestheatre.util.MockResponseFileReader
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Named

@ExperimentalCoroutinesApi
@SmallTest
@HiltAndroidTest
class DetailRepositoryTest {
    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    lateinit var mockWebServer: MockWebServer

    @Inject
    @Named("test_retrofit")
    lateinit var retrofitBuilder: Retrofit.Builder

    @Inject
    @Named("test_db")
    lateinit var database: AppDatabase

    private lateinit var detailRepository: DetailRepository
    private lateinit var detailDao: DetailDao
    private lateinit var providerDao: ProviderDao
    private lateinit var cinemaWorldApi: CinemaWorldApi
    private lateinit var filmWorldApi: FilmWorldApi

    @Before
    fun start() {
        hiltRule.inject()

        detailDao = database.detailDao()
        providerDao = database.providerDao()

        mockWebServer.start(MOCK_WEB_SERVER_PORT)
        cinemaWorldApi = retrofitBuilder
            .baseUrl(mockWebServer.url("/"))
            .build()
            .create(CinemaWorldApi::class.java)

        filmWorldApi = retrofitBuilder
            .baseUrl(mockWebServer.url("/"))
            .build()
            .create(FilmWorldApi::class.java)

        detailRepository = DetailRepository(
            detailDao, providerDao, cinemaWorldApi, filmWorldApi
        )
    }

    @After
    fun teardown() {
        database.close()
        mockWebServer.shutdown()
    }

    private val media = DBMedia(
        "Star Wars: Episode VII - The Force Awakens",
        "https://m.media-amazon.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SX300.jpg",
        MediaType.Movie
    )

    private val providerList: List<DBMediaProvider> = listOf(
        DBMediaProvider(media.id, "cw2488496", ProviderName.CinemaWorld),
        DBMediaProvider(media.id, "fw2488496", ProviderName.FilmWorld)
    )

    //Good API request
    @Test
    fun fetchDetailFromCinemaWorldApiGood() = runBlocking {
        mockWebServer.apply {
            enqueue(MockResponse().setBody(
                MockResponseFileReader("cinema_world_detail.json").content
            ))
        }

        val detail: MediaDetail? = detailRepository.fetchDetailFromCinemaWorldApi("cw2488496", media.id)
        assertThat(detail).isNotNull()

        val dbMediaProviders: List<DBMediaProvider> = providerDao.getProviders(media.id)
        assertThat(dbMediaProviders).hasSize(1)
        assertThat(dbMediaProviders[0].movieId).matches(media.id)

        assertThat(detail!!.title).matches("Star Wars: Episode VII - The Force Awakens")
    }

    //Bad API request
    @Test
    fun fetchDetailFromCinemaWorldApiBad() = runBlocking {
        mockWebServer.apply {
            enqueue(MockResponse().setBody(
                MockResponseFileReader("detail_error.json").content
            ).setResponseCode(502))
        }

        providerDao.insert(providerList[0])

        val detail: MediaDetail? = detailRepository.fetchDetailFromCinemaWorldApi("cw2488496", media.id)
        assertThat(detail).isNull()

        val dbMediaProvider: List<DBMediaProvider> = providerDao.getProviders(media.id)
        assertThat(dbMediaProvider).hasSize(1)
        assertThat(dbMediaProvider[0].price).isNull()
    }

    //Test fetch with 2 good APIs on a blank DB
    @Test
    fun fetchDetailGoodBlank() = runBlocking {
        mockWebServer.apply {
            enqueue(MockResponse().setBody(
                MockResponseFileReader("cinema_world_detail.json").content
            ))
        }
        mockWebServer.apply {
            enqueue(MockResponse().setBody(
                MockResponseFileReader("film_world_detail.json").content
            ))
        }

        providerDao.insert(*providerList.toTypedArray())

        val mediaDetail: MediaDetail? = detailRepository.fetchDetails(media.id)
        assertThat(mediaDetail).isNotNull()

        val dbProviders = providerDao.getProviders(media.id)
        assertThat(dbProviders).hasSize(2)
        assertThat(dbProviders[0].price).isNotNull()

        val dbDetail: DBDetail? = detailDao.getDetail(media.id)
        assertThat(dbDetail).isNotNull()
        assertThat(dbDetail!!.title).matches("Star Wars: Episode VII - The Force Awakens")

        assertThat(mediaDetail!!.title).matches("Star Wars: Episode VII - The Force Awakens")
        assertThat(mediaDetail.prices).isNotNull()
        assertThat(mediaDetail.prices).hasSize(2)
    }

    //Test fetch with 1 bad API on a blank DB
    @Test
    fun fetchDetailOneBadBlank() = runBlocking {
        mockWebServer.apply {
            enqueue(MockResponse().setBody(
                MockResponseFileReader("detail_error.json").content
            ).setResponseCode(502))
        }
        mockWebServer.apply {
            enqueue(MockResponse().setBody(
                MockResponseFileReader("film_world_detail.json").content
            ))
        }

        providerDao.insert(*providerList.toTypedArray())

        val mediaDetail: MediaDetail? = detailRepository.fetchDetails(media.id)
        assertThat(mediaDetail).isNotNull()

        val dbProviders = providerDao.getProviders(media.id)
        assertThat(dbProviders).hasSize(2)
        assertThat(dbProviders[0].price).isNull()
        assertThat(dbProviders[1].price).isNotNull()

        val dbDetail: DBDetail? = detailDao.getDetail(media.id)
        assertThat(dbDetail).isNotNull()
        assertThat(dbDetail!!.title).matches("Star Wars: Episode VII - The Force Awakens")

        assertThat(mediaDetail!!.title).matches("Star Wars: Episode VII - The Force Awakens")
        assertThat(mediaDetail.prices).isNotNull()
        assertThat(mediaDetail.prices).hasSize(2)
    }

    //Test fetch with 2 bad APIs on a blank DB
    @Test
    fun fetchDetailTwoBadBlank() = runBlocking {
        mockWebServer.apply {
            enqueue(MockResponse().setBody(
                MockResponseFileReader("detail_error.json").content
            ).setResponseCode(502))
        }
        mockWebServer.apply {
            enqueue(MockResponse().setBody(
                MockResponseFileReader("detail_error.json").content
            ).setResponseCode(502))
        }

        providerDao.insert(*providerList.toTypedArray())

        val mediaDetail: MediaDetail? = detailRepository.fetchDetails(media.id)
        assertThat(mediaDetail).isNull()

        val dbDetail: DBDetail? = detailDao.getDetail(media.id)
        assertThat(dbDetail).isNull()
    }
}
