package com.johnny.princestheatre.repository

import android.content.Context
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import com.johnny.princestheatre.database.AppDatabase
import com.johnny.princestheatre.database.dao.DetailDao
import com.johnny.princestheatre.database.dao.MediaDao
import com.johnny.princestheatre.database.dao.ProviderDao
import com.johnny.princestheatre.database.entity.DBMedia
import com.johnny.princestheatre.database.entity.DBMediaProvider
import com.johnny.princestheatre.database.entity.ProviderName
import com.johnny.princestheatre.di.TestNetworkModule.MOCK_WEB_SERVER_PORT
import com.johnny.princestheatre.network.api.CinemaWorldApi
import com.johnny.princestheatre.network.api.FilmWorldApi
import com.johnny.princestheatre.util.MockResponseFileReader
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Named

@ExperimentalCoroutinesApi
@SmallTest
@HiltAndroidTest
class MovieRepositoryTest {
    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    @Inject
    @ApplicationContext
    lateinit var applicationContext: Context

    @Inject
    lateinit var mockWebServer: MockWebServer

    @Inject
    @Named("test_retrofit")
    lateinit var retrofitBuilder: Retrofit.Builder

    @Inject
    @Named("test_db")
    lateinit var database: AppDatabase

    private lateinit var movieRepository: MovieRepository
    private lateinit var mediaDao: MediaDao
    private lateinit var providerDao: ProviderDao
    private lateinit var detailDao: DetailDao
    private lateinit var cinemaWorldApi: CinemaWorldApi
    private lateinit var filmWorldApi: FilmWorldApi

    @Before
    fun start() {
        hiltRule.inject()

        mediaDao = database.mediaDao()
        providerDao = database.providerDao()
        detailDao = database.detailDao()

        mockWebServer.start(MOCK_WEB_SERVER_PORT)
        cinemaWorldApi = retrofitBuilder
            .baseUrl(mockWebServer.url("/"))
            .build()
            .create(CinemaWorldApi::class.java)

        filmWorldApi = retrofitBuilder
            .baseUrl(mockWebServer.url("/"))
            .build()
            .create(FilmWorldApi::class.java)

        movieRepository = MovieRepository(applicationContext, mediaDao, providerDao, detailDao, cinemaWorldApi, filmWorldApi)
    }

    @After
    fun teardown() {
        database.close()
        mockWebServer.shutdown()
    }

    @Test
    fun badMovieRequest() = runBlocking {
        mockWebServer.apply {
            enqueue(MockResponse().setBody(
                MockResponseFileReader("detail_error.json").content
            ).setResponseCode(502))
        }

        val media: List<DBMedia>? = movieRepository.fetchCinemaWorldMovieList()
        assertThat(media).isNull()
    }

    @Test
    fun goodMovieRequest() = runBlocking {
        mockWebServer.apply {
            enqueue(MockResponse().setBody(
                MockResponseFileReader("cinema_world_movies.json").content
            ))
        }

        providerDao.insert(DBMediaProvider("movie-id","provider-id",ProviderName.CinemaWorld))

        val media: List<DBMedia>? = movieRepository.fetchCinemaWorldMovieList()
        assertThat(media).isNotNull()
        assertThat(media).hasSize(11)

        val dbMediaProvider = providerDao.getProviders("movie-id")
        assertThat(dbMediaProvider).isEmpty()

        val dbMediaList = mediaDao.selectAll()
        assertThat(dbMediaList).hasSize(11)

        assertThat(media!![0].providerId).matches("cw2488496")
    }
}
