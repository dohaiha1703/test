package com.johnny.princestheatre.database.dao

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import com.johnny.princestheatre.database.AppDatabase
import com.johnny.princestheatre.database.entity.*
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject
import javax.inject.Named

@ExperimentalCoroutinesApi
@SmallTest
@HiltAndroidTest
class DetailDaoTest {
    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    @Named("test_db")
    lateinit var database: AppDatabase

    private lateinit var detailDao: DetailDao

    @Before
    fun setup() {
        hiltRule.inject()
        detailDao = database.detailDao()
    }

    @After
    fun teardown() {
        database.close()
    }

    private val media = DBMedia("title-1", "https://example.com/image1.png", MediaType.Movie)

    private val detailList: List<DBDetail> = listOf(
        DBDetail(
            media.id, "title-1", 2021, "rated-1", "released-1", "runtime-1",
            "genre-1", "director-1", "writer-1", "actor-1", "plot-1", "lang-1",
            "country-1", "poster-1", "movie", "production-1"
        ),
        DBDetail(
            "movie-id-2", "title-2", 2021, "rated-2", "released-2", "runtime-2",
            "genre-2", "director-2", "writer-2", "actor-2", "plot-2", "lang-2",
            "country-2", "poster-2", "movie", "production-2"
        )
    )

    @Test
    fun insertDetail() = runBlockingTest {
        detailDao.insert(*detailList.toTypedArray())
        val dbDetail: DBDetail? = detailDao.getDetail(detailList[0].movieId)
        assertThat(dbDetail).isEqualTo(detailList[0])
    }

    @Test
    fun deleteOrphans() = runBlockingTest {
        val mediaDao = database.mediaDao()

        mediaDao.insert(media)
        detailDao.insert(*detailList.toTypedArray())
        detailDao.deleteOrphans()

        var dbDetail = detailDao.getDetail(detailList[0].movieId)
        assertThat(dbDetail).isEqualTo(detailList[0])
        dbDetail = detailDao.getDetail(detailList[1].movieId)
        assertThat(dbDetail).isNull()
    }
}
