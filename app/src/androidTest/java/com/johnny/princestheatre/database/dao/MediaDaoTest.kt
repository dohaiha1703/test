package com.johnny.princestheatre.database.dao

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import com.johnny.princestheatre.database.AppDatabase
import com.johnny.princestheatre.database.entity.DBMedia
import com.johnny.princestheatre.database.entity.DBMediaProvider
import com.johnny.princestheatre.database.entity.MediaType
import com.johnny.princestheatre.database.entity.ProviderName
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject
import javax.inject.Named

@ExperimentalCoroutinesApi
@SmallTest
@HiltAndroidTest
class MediaDaoTest {
    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    @Named("test_db")
    lateinit var database: AppDatabase

    private lateinit var mediaDao: MediaDao

    @Before
    fun setup() {
        hiltRule.inject()
        mediaDao = database.mediaDao()
    }

    @After
    fun teardown() {
        database.close()
    }

    private val mediaList: List<DBMedia> = listOf(
        DBMedia("Test title 1", "https://example.com/image1.png", MediaType.Movie),
        DBMedia("Test title 2", "https://example.com/image2.png", MediaType.Movie)
    )

    @Test
    fun insertMedia() = runBlockingTest {
        mediaDao.insert(*mediaList.toTypedArray())

        val dbMediaList = mediaDao.selectAll()
        assertThat(dbMediaList).containsExactlyElementsIn(mediaList)
    }

    @Test
    fun deleteOrphans() = runBlockingTest {
        val providerDao: ProviderDao = database.providerDao()
        val mediaProvider = DBMediaProvider(mediaList[0].id, "test_id", ProviderName.CinemaWorld)

        mediaDao.insert(*mediaList.toTypedArray())
        providerDao.insert(mediaProvider)
        mediaDao.deleteOrphans()

        val dbMediaList = mediaDao.selectAll()
        assertThat(dbMediaList).containsExactly(mediaList[0])
    }
}
