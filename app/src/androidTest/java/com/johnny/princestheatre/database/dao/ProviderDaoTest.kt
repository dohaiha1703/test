package com.johnny.princestheatre.database.dao

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import com.johnny.princestheatre.database.AppDatabase
import com.johnny.princestheatre.database.entity.DBMediaProvider
import com.johnny.princestheatre.database.entity.ProviderName
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject
import javax.inject.Named

@ExperimentalCoroutinesApi
@SmallTest
@HiltAndroidTest
class ProviderDaoTest {
    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    @Named("test_db")
    lateinit var database: AppDatabase

    private lateinit var providerDao: ProviderDao

    @Before
    fun setup() {
        hiltRule.inject()
        providerDao = database.providerDao()
    }

    @After
    fun teardown() {
        database.close()
    }

    private val providerList: List<DBMediaProvider> = listOf(
        DBMediaProvider("movie-id-1", "provider-id-1", ProviderName.CinemaWorld),
        DBMediaProvider("movie-id-1", "provider-id-2", ProviderName.FilmWorld),
        DBMediaProvider("movie-id-2", "provider-id-3", ProviderName.CinemaWorld)
    )

    @Test
    fun insert() = runBlockingTest {
        providerDao.insert(*providerList.toTypedArray())
        var dbProviderList: List<DBMediaProvider> = providerDao.getProviders("movie-id-1")
        assertThat(dbProviderList).containsExactlyElementsIn(providerList.filter { it.movieId == "movie-id-1" })
        dbProviderList = providerDao.getProviders("movie-id-2")
        assertThat(dbProviderList).containsExactlyElementsIn(providerList.filter { it.movieId == "movie-id-2" })
    }

    @Test
    fun delete() = runBlockingTest {
        providerDao.insert(*providerList.toTypedArray())
        providerDao.deleteProvider(ProviderName.CinemaWorld)
        var dbProviderList: List<DBMediaProvider> = providerDao.getProviders("movie-id-1")
        assertThat(dbProviderList).containsExactlyElementsIn(providerList.filter {
            it.movieId == "movie-id-1" && it.provider != ProviderName.CinemaWorld
        })
        dbProviderList = providerDao.getProviders("movie-id-2")
        assertThat(dbProviderList).isEmpty()
    }
}