package com.johnny.princestheatre.model

import com.johnny.princestheatre.database.entity.ProviderName

data class MediaDetail(
    val movieId: String,
    val title: String,
    val year: Int,
    val rated: String,
    val released: String,
    val runtime: String,
    val genre: String,
    val director: String,
    val writer: String,
    val actors: String,
    val plot: String,
    val language: String,
    val country: String,
    val poster: String,
    val type: String,
    val production: String
) {
    var prices: List<MediaDetailPrice>? = null
}

data class MediaDetailPrice(
    val providerName: ProviderName,
    val price: Double?
)
