package com.johnny.princestheatre.model

data class Movie(
    val id: String,
    val title: String,
    val poster: String
)
