package com.johnny.princestheatre.network.api

import com.johnny.princestheatre.BuildConfig
import com.johnny.princestheatre.network.entity.TestMovieDetailResponse
import com.johnny.princestheatre.network.entity.TestMovieResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface CinemaWorldApi {
    companion object {
        const val BASE_URL: String = "https://challenge.lexicondigital.com.au/api/cinemaworld/"
    }

    @Headers(
        "x-api-key: ${BuildConfig.TEST_API_KEY}"
    )
    @GET("movies")
    suspend fun getAllMovies(): Response<TestMovieResponse>

    @Headers(
        "x-api-key: ${BuildConfig.TEST_API_KEY}"
    )
    @GET("movie/{id}")
    suspend fun getMovieDetails(
        @Path("id") id: String
    ): Response<TestMovieDetailResponse>
}
