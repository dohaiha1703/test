package com.johnny.princestheatre.network.entity

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/*
    Response object from test APIs
    Real world movie databases will be in differing formats that don't necessarily align well
 */

@JsonClass(generateAdapter = true)
data class TestMovieResponse(
    @Json(name = "Provider") val provider: String,
    @Json(name = "Movies") val movies: List<TestMovie>
)

@JsonClass(generateAdapter = true)
data class TestMovie(
    @Json(name = "ID") val id: String,
    @Json(name = "Title") val title: String,
    @Json(name = "Type") val type: String,
    @Json(name = "Poster") val poster: String
)
