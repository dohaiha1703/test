package com.johnny.princestheatre.util

/*
    A utility class to prevent livedata from being used multiple times
 */
class LiveDataEvent<out T>(
    private val content: T
) {
    var hasBeenHandled: Boolean = false
        private set

    // Returns the content and prevents its use again
    fun getContentIfNotHandled(): T? {
        if (hasBeenHandled)
            return null

        hasBeenHandled = true
        return content
    }

    // Returns the content, even if it's already been used
    fun peekContent(): T = content
}