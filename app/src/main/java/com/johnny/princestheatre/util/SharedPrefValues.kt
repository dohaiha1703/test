package com.johnny.princestheatre.util

const val SHARED_PREF_PRIVATE_MODE: Int = 0
const val SHARED_PREF_LAST_SYNC: String = "princes_theatre_last_sync"
const val LAST_SYNC_TIME: String = "last_sync_time"