package com.johnny.princestheatre.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.view.View
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.google.android.material.button.MaterialButton
import com.johnny.princestheatre.R
import com.johnny.princestheatre.model.MediaDetail
import com.johnny.princestheatre.ui.viewmodel.DetailViewModel
import com.johnny.princestheatre.util.LiveDataEvent
import dagger.hilt.android.AndroidEntryPoint
import java.text.NumberFormat

@AndroidEntryPoint
class DetailActivity: AppCompatActivity() {
    private val viewModel: DetailViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        //Collect the media id from the movie activity on first create
        if (intent.extras != null) {
            viewModel.mediaId = intent.extras!!.getString("MEDIA_ID")
            intent.removeExtra("MEDIA_ID")
        }

        //Let the movie activity know that no errors occurred
        findViewById<MaterialButton>(R.id.backButton).setOnClickListener {
            setResult(Activity.RESULT_OK)
            finish()
        }

        viewModel.mediaDetailLiveData.observe(this, { event: LiveDataEvent<DetailViewModel.DetailContent> ->
            event.getContentIfNotHandled()?.let { content: DetailViewModel.DetailContent ->
                if (content.mediaDetail != null) {
                    showDetails(content.mediaDetail)
                    //Hide loading container
                    findViewById<ScrollView>(R.id.detailScrollCon).visibility = View.VISIBLE
                    findViewById<LinearLayout>(R.id.detailLoadingCon).visibility = View.GONE
                }
                else {
                    //Let movie activity know that an error occurred
                    setResult(Activity.RESULT_CANCELED)
                    finish()
                }
            }
        })

        viewModel.getDetails()
    }

    /*
        Populate the page with the collected activities.
        Would be nice to have some default and broken placeholder images loaded with Glide.
     */
    private fun showDetails(details: MediaDetail) {
        Glide.with(this)
            .load(details.poster)
            .transform(
                CenterInside(),
                RoundedCorners(15)
            )
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .into(findViewById(R.id.moviePosterImage))

        Glide.with(this)
            .load(details.poster)
            .transform(
                CenterCrop(),
            )
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .into(findViewById(R.id.movieBannerImage))

        findViewById<TextView>(R.id.movieTitleText).text = details.title
        findViewById<TextView>(R.id.movieYearText).text = details.year.toString()
        findViewById<TextView>(R.id.movieRatingText).text = details.rated
        findViewById<TextView>(R.id.movieRuntimeText).text = details.runtime
        findViewById<TextView>(R.id.moviePlotText).text = details.plot

        if (!details.prices.isNullOrEmpty()) {
            val format = NumberFormat.getCurrencyInstance()
            format.maximumFractionDigits = 2
            format.minimumFractionDigits = 2
            val formattedPrice = format.format(details.prices!![0].price)

            findViewById<TextView>(R.id.movieProviderText).text = details.prices!![0].providerName.v
            findViewById<TextView>(R.id.moviePriceText).text = Html.fromHtml("<b>$formattedPrice</b> (${getText(R.string.cheapest)})", Html.FROM_HTML_MODE_LEGACY)
        }

        //Write is as a joined list for cleanliness
        val detailsText = listOf<String>(
            "<b>${getText(R.string.released)}:</b> ${details.released}",
            "<b>${getText(R.string.runtime)}:</b> ${details.runtime}",
            "<b>${getText(R.string.genre)}:</b> ${details.genre}",
            "<b>${getText(R.string.director)}:</b> ${details.director}",
            "<b>${getText(R.string.writer)}:</b> ${details.writer}",
            "<b>${getText(R.string.actors)}:</b> ${details.actors}",
            "<b>${getText(R.string.production)}:</b> ${details.production}"
        ).joinToString("<br>")

        findViewById<TextView>(R.id.movieDetailsText).text = Html.fromHtml(detailsText, Html.FROM_HTML_MODE_LEGACY)
    }
}
