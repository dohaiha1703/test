package com.johnny.princestheatre.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.johnny.princestheatre.R
import com.johnny.princestheatre.model.Movie
import java.util.*
import javax.inject.Inject

class MediaListAdapter
@Inject
constructor(): RecyclerView.Adapter<MediaListAdapter.ViewHolder>(), Filterable {
    //One master list of all movies, one list for the filtered items that gets displayed
    private var items: List<Movie> = listOf()
    private var filteredItems: MutableList<Movie> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_media, parent, false),
            parent.context
        )
    }

    override fun getItemCount(): Int = filteredItems.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(filteredItems[position])
    }

    override fun getFilter(): Filter {
        //Filter with the text collected from the textview in the details activity.
        return object: Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                //Make both the filter text and the movie title text lowercase to search regardless of case
                val searchStr = constraint.toString().toLowerCase(Locale.getDefault())
                if (searchStr.isEmpty()) {
                    filteredItems = items.toMutableList()
                }
                else {
                    filteredItems.clear()
                    items.forEach { movie: Movie ->
                        if (movie.title.toLowerCase(Locale.getDefault()).contains(searchStr))
                            filteredItems.add(movie)
                    }
                }

                val results = FilterResults()
                results.values = filteredItems
                return results
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence, results: FilterResults) {
                filteredItems = results.values as MutableList<Movie>
                notifyDataSetChanged()
            }

        }
    }

    fun submitList(list: List<Movie>) {
        items = list
        filteredItems = list.toMutableList()
        notifyDataSetChanged()
    }

    class ViewHolder(
        itemView: View,
        private val parentContext: Context
    ): RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private val titleText: TextView = itemView.findViewById(R.id.mediaTitleText)
        private val posterImage: ImageView = itemView.findViewById(R.id.mediaPosterImage)

        private lateinit var movieId: String

        init {
            //make entire item clickable
            itemView.setOnClickListener(this)
        }

        fun bind(movie: Movie) {
            movieId = movie.id
            titleText.text = movie.title
            //Load image from url and cache the untouched image
            Glide.with(itemView)
                .load(movie.poster)
                .transform(
                    CenterCrop(),
                    RoundedCorners(15)
                )
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .into(posterImage)
        }

        override fun onClick(v: View?) {
            //If the parent activity can handle the item taps
            if (parentContext is MediaListListener)
                parentContext.onMediaTaped(movieId)
        }
    }

    interface MediaListListener {
        fun onMediaTaped(mediaId: String)
    }
}
