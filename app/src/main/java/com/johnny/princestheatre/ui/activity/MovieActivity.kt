package com.johnny.princestheatre.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.johnny.princestheatre.R
import com.johnny.princestheatre.ui.adapter.MediaListAdapter
import com.johnny.princestheatre.ui.viewmodel.MovieViewModel
import com.johnny.princestheatre.util.GridAutofitLayoutManager
import com.johnny.princestheatre.util.LiveDataEvent
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MovieActivity: AppCompatActivity(), MediaListAdapter.MediaListListener {
    private val viewModel: MovieViewModel by viewModels()

    @Inject
    lateinit var movieListAdapter: MediaListAdapter

    private lateinit var mediaSearch: EditText
    private lateinit var swipeContainer: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        //Hide splash screen once activity ready
        setTheme(R.style.Theme_MaterialComponents_Light_NoActionBar)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie)

        mediaSearch = findViewById(R.id.movieSearch)
        swipeContainer = findViewById(R.id.movieSwipeCon)
        initRecyclerView()

        viewModel.movieListLiveData.observe(this, { event: LiveDataEvent<MovieViewModel.MediaContent> ->
            event.getContentIfNotHandled()?.let { content: MovieViewModel.MediaContent ->
                if (!content.movieList.isNullOrEmpty()) {
                    movieListAdapter.submitList(content.movieList)
                    mediaSearch.setText(viewModel.filterText)
                }
                else {
                    showSnack(getString(R.string.movieListFail))
                }
                swipeContainer.isRefreshing = false
            }
        })

        viewModel.getMovieList()
    }

    //If there's an issue getting the details, let this activity know so a message can be displayed
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_CANCELED) {
            showSnack(getString(R.string.detailFailMsg))
        }
    }

    private fun initRecyclerView() {
        //Make the recyclerview a grid that resizes elements to fit the screen best
        val recyclerView: RecyclerView = findViewById(R.id.movieRecyclerView)
        recyclerView.layoutManager = GridAutofitLayoutManager(this, 400)
        recyclerView.adapter = movieListAdapter

        //Listen to text change to trigger the recyclerview to filter based on title
        mediaSearch.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                viewModel.filterText = s
                movieListAdapter.filter.filter(s)
            }

            //Useless overrides
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable?) {}
        })

        //Pull to refresh the recyclerview only from API
        swipeContainer.setOnRefreshListener {
            viewModel.refreshMovieList()
        }
    }

    //Send movie id to detail activity so the repository knows what to call for
    override fun onMediaTaped(mediaId: String) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra("MEDIA_ID", mediaId)
        startActivityForResult(intent, 0)
    }

    private fun showSnack(message: String) {
        val snack = Snackbar.make(swipeContainer, message, Snackbar.LENGTH_LONG)
        snack.show()
    }
}
