package com.johnny.princestheatre.ui.viewmodel

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.johnny.princestheatre.model.Movie
import com.johnny.princestheatre.repository.MovieRepository
import com.johnny.princestheatre.util.LiveDataEvent
import com.johnny.princestheatre.util.TAG
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MovieViewModel
@ViewModelInject
constructor(
    private val movieRepository: MovieRepository
): ViewModel() {
    private var movieList: List<Movie>? = null

    var filterText: CharSequence = ""

    private val _movieListLiveData: MutableLiveData<LiveDataEvent<MediaContent>> = MutableLiveData()
    val movieListLiveData: LiveData<LiveDataEvent<MediaContent>>
        get() = _movieListLiveData

    fun getMovieList() {
        if (movieList != null) {
            _movieListLiveData.postValue(LiveDataEvent(MediaContent(movieList, null)))
            return
        }

        viewModelScope.launch(Dispatchers.IO) {
            val request = kotlin.runCatching {
                movieRepository.fetchMovieList()
            }
            request.onSuccess {
                movieList = it
                _movieListLiveData.postValue(LiveDataEvent(MediaContent(it, null)))
            }
            request.onFailure { e: Throwable ->
                Log.e(TAG, "Error fetching movie list", e)
                _movieListLiveData.postValue(LiveDataEvent(MediaContent(null, e)))
            }
        }
    }

    fun refreshMovieList() {
        viewModelScope.launch(Dispatchers.IO) {
            val request = kotlin.runCatching {
                movieRepository.refreshMovieList()
            }
            request.onSuccess {
                movieList = it
                _movieListLiveData.postValue(LiveDataEvent(MediaContent(it, null)))
            }
            request.onFailure { e: Throwable ->
                Log.e(TAG, "Error refreshing movie list", e)
                _movieListLiveData.postValue(LiveDataEvent(MediaContent(null, e)))
            }
        }
    }

    data class MediaContent(
        val movieList: List<Movie>?,
        val e: Throwable?
    )
}
