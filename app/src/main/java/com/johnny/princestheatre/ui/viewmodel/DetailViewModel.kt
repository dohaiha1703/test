package com.johnny.princestheatre.ui.viewmodel

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.johnny.princestheatre.model.MediaDetail
import com.johnny.princestheatre.repository.DetailRepository
import com.johnny.princestheatre.util.LiveDataEvent
import com.johnny.princestheatre.util.TAG
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailViewModel
@ViewModelInject
constructor(
    private val detailRepository: DetailRepository
): ViewModel() {
    var mediaId: String? = null
    private var mediaDetail: MediaDetail? = null

    //Private mutable livedata so only this class can modify it
    private val _mediaDetail: MutableLiveData<LiveDataEvent<DetailContent>> = MutableLiveData()
    val mediaDetailLiveData: LiveData<LiveDataEvent<DetailContent>>
        get() = _mediaDetail

    fun getDetails() {
        if (mediaDetail != null) {
            _mediaDetail.postValue(LiveDataEvent(DetailContent(mediaDetail, null)))
            return
        }
        viewModelScope.launch(Dispatchers.IO) {
            val request = kotlin.runCatching {
                detailRepository.fetchDetails(mediaId!!)
            }
            request.onSuccess {
                mediaDetail = it
                _mediaDetail.postValue(LiveDataEvent(DetailContent(it, null)))
            }
            request.onFailure { e: Throwable ->
                Log.e(TAG, "Failed to get media details", e)
                _mediaDetail.postValue(LiveDataEvent(DetailContent(null, e)))
            }
        }
    }

    data class DetailContent (
        val mediaDetail: MediaDetail?,
        val e: Throwable?
    )
}
