package com.johnny.princestheatre.database

import androidx.room.TypeConverter
import com.johnny.princestheatre.database.entity.DBMediaProvider
import com.johnny.princestheatre.database.entity.MediaType
import com.johnny.princestheatre.database.entity.ProviderName
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import java.lang.reflect.Type

/*
    Type converters for Moshi
 */

object DBTypeConverters {
    private lateinit var moshi: Moshi

    fun initialise(moshi: Moshi) {
        this.moshi = moshi

        dbMovieProviderAdapter = moshi.adapter(listDbMovieProvider)
    }

    private val listDbMovieProvider: Type = Types.newParameterizedType(List::class.java, DBMediaProvider::class.java)
    private lateinit var dbMovieProviderAdapter: JsonAdapter<List<DBMediaProvider>>

    @TypeConverter
    @JvmStatic
    fun fromDbMovieProvider(value: List<DBMediaProvider>?): String {
        return dbMovieProviderAdapter.toJson(value)
    }

    @TypeConverter
    @JvmStatic
    fun toDbMovieProvider(value: String?): List<DBMediaProvider>? {
        if (value == null) return null
        return dbMovieProviderAdapter.fromJson(value)
    }

    @TypeConverter
    @JvmStatic
    fun toMediaType(value: String): MediaType {
        return MediaType.values().first { it.v == value }
    }

    @TypeConverter
    @JvmStatic
    fun fromMediaType(value: MediaType): String {
        return value.v
    }

    @TypeConverter
    @JvmStatic
    fun toProviderName(value: String): ProviderName {
        return ProviderName.values().first { it.v == value }
    }

    @TypeConverter
    @JvmStatic
    fun fromProviderName(value: ProviderName): String {
        return value.v
    }
}
