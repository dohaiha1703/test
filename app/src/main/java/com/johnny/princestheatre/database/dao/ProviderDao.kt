package com.johnny.princestheatre.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.johnny.princestheatre.database.entity.DBMediaProvider
import com.johnny.princestheatre.database.entity.ProviderName

@Dao
interface ProviderDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg provider: DBMediaProvider)

    @Query("SELECT * FROM provider WHERE movieId = :movieId ORDER BY price ASC")
    suspend fun getProviders(movieId: String): List<DBMediaProvider>

    @Query("DELETE FROM provider WHERE provider = :providerName")
    suspend fun deleteProvider(providerName: ProviderName)
}
