package com.johnny.princestheatre.database.entity

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.*

enum class MediaType(val v: String) {
    Movie("movie"),
    Unknown("unknown")
}

@Entity(
    tableName = "media",
    indices = [Index(value = ["title"], unique = true)]
)
data class DBMedia(
    val title: String,
    val poster: String,
    val type: MediaType,
) {
    @PrimaryKey
    var id: String = UUID.randomUUID().toString()
    @Ignore
    var providerId: String = ""
}
