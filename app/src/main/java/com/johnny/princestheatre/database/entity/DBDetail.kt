package com.johnny.princestheatre.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass
import java.util.*

@Entity(tableName = "detail")
data class DBDetail(
    val movieId: String,
    val title: String,
    val year: Int,
    val rated: String,
    val released: String,
    val runtime: String,
    val genre: String,
    val director: String,
    val writer: String,
    val actors: String,
    val plot: String,
    val language: String,
    val country: String,
    val poster: String,
    val type: String,
    val production: String,
) {
    @PrimaryKey
    var id: String = UUID.randomUUID().toString()
}
