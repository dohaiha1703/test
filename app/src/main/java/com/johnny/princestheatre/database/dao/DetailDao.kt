package com.johnny.princestheatre.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.johnny.princestheatre.database.entity.DBDetail

@Dao
interface DetailDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(vararg detail: DBDetail)

    @Query("SELECT * FROM detail WHERE movieId = :movieId")
    suspend fun getDetail(movieId: String): DBDetail?

    @Query("DELETE FROM detail WHERE movieId NOT IN (SELECT id FROM media)")
    suspend fun deleteOrphans()
}
