package com.johnny.princestheatre.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.johnny.princestheatre.database.entity.DBMedia

@Dao
interface MediaDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(vararg media: DBMedia)

    @Query("SELECT * FROM media ORDER BY title")
    suspend fun selectAll(): List<DBMedia>

    @Query("DELETE FROM media WHERE id NOT IN (SELECT DISTINCT movieId FROM provider)")
    suspend fun deleteOrphans()
}
