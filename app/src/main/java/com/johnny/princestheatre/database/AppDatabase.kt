package com.johnny.princestheatre.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.johnny.princestheatre.database.dao.DetailDao
import com.johnny.princestheatre.database.dao.MediaDao
import com.johnny.princestheatre.database.dao.ProviderDao
import com.johnny.princestheatre.database.entity.DBDetail
import com.johnny.princestheatre.database.entity.DBMedia
import com.johnny.princestheatre.database.entity.DBMediaProvider

@Database(
    entities = [DBMedia::class, DBDetail::class, DBMediaProvider::class],
    version = 1
)
@TypeConverters(DBTypeConverters::class)
abstract class AppDatabase: RoomDatabase() {
    abstract fun mediaDao(): MediaDao
    abstract fun detailDao(): DetailDao
    abstract fun providerDao(): ProviderDao

    companion object {
        const val DB_NAME: String = "prince.db"
    }
}
