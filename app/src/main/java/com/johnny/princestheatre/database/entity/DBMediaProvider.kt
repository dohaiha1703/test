package com.johnny.princestheatre.database.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass
import java.util.*
import java.util.function.DoubleToLongFunction

enum class ProviderName(val v: String) {
    CinemaWorld("Cinema World"),
    FilmWorld("Film World")
}

@Entity(
    tableName = "provider",
    indices = [Index(value = ["movieId", "provider"], unique = true)]
)
@JsonClass(generateAdapter = true)
data class DBMediaProvider(
    val movieId: String,
    val providerId: String,
    val provider: ProviderName
) {
    @PrimaryKey
    var id: String = UUID.randomUUID().toString()
    var price: Double? = null
}
