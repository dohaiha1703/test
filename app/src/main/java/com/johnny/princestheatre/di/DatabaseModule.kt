package com.johnny.princestheatre.di

import android.content.Context
import androidx.room.Room
import com.johnny.princestheatre.database.AppDatabase
import com.johnny.princestheatre.database.DBTypeConverters
import com.johnny.princestheatre.database.dao.DetailDao
import com.johnny.princestheatre.database.dao.MediaDao
import com.johnny.princestheatre.database.dao.ProviderDao
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object DatabaseModule {
    @Singleton
    @Provides
    fun provideMoshiBuilder(): Moshi {
        return Moshi.Builder().build()
    }

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context, moshi: Moshi): AppDatabase {
        DBTypeConverters.initialise(moshi)
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            AppDatabase.DB_NAME
        ).build()
    }

    @Singleton
    @Provides
    fun provideMovieDao(appDb: AppDatabase): MediaDao {
        return appDb.mediaDao()
    }

    @Singleton
    @Provides
    fun provideDetailDao(appDb: AppDatabase): DetailDao {
        return appDb.detailDao()
    }

    @Singleton
    @Provides
    fun provideProviderDao(appDb: AppDatabase): ProviderDao {
        return appDb.providerDao()
    }
}
