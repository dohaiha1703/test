package com.johnny.princestheatre.di

import com.johnny.princestheatre.network.interceptor.RetryInterceptor
import com.johnny.princestheatre.network.api.CinemaWorldApi
import com.johnny.princestheatre.network.api.FilmWorldApi
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object NetworkModule {
    @Singleton
    @Provides
    fun provideOkHttpClient(retryInterceptor: RetryInterceptor): OkHttpClient {
        val loggingInterceptor  = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(retryInterceptor)
            .build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(client: OkHttpClient, moshi: Moshi): Retrofit.Builder {
        return Retrofit.Builder()
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
    }

    @Singleton
    @Provides
    fun provideCinemaWorldApi(retrofit: Retrofit.Builder): CinemaWorldApi {
        return retrofit
            .baseUrl(CinemaWorldApi.BASE_URL)
            .build()
            .create(CinemaWorldApi::class.java)
    }

    @Singleton
    @Provides
    fun provideFilmWorldApi(retrofit: Retrofit.Builder): FilmWorldApi {
        return retrofit
            .baseUrl(FilmWorldApi.BASE_URL)
            .build()
            .create(FilmWorldApi::class.java)
    }
}
