package com.johnny.princestheatre.di

import android.content.Context
import com.johnny.princestheatre.database.dao.DetailDao
import com.johnny.princestheatre.database.dao.MediaDao
import com.johnny.princestheatre.database.dao.ProviderDao
import com.johnny.princestheatre.network.api.CinemaWorldApi
import com.johnny.princestheatre.network.api.FilmWorldApi
import com.johnny.princestheatre.repository.DetailRepository
import com.johnny.princestheatre.repository.MovieRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RepositoryModule {
    @Singleton
    @Provides
    fun provideMovieRepository(
        @ApplicationContext context: Context,
        mediaDao: MediaDao,
        providerDao: ProviderDao,
        detailDao: DetailDao,
        cinemaWorldApi: CinemaWorldApi,
        filmWorldApi: FilmWorldApi
    ): MovieRepository {
        return MovieRepository(
            context, mediaDao, providerDao, detailDao, cinemaWorldApi, filmWorldApi
        )
    }

    @Singleton
    @Provides
    fun provideDetailRepository(
        detailDao: DetailDao,
        providerDao: ProviderDao,
        cinemaWorldApi: CinemaWorldApi,
        filmWorldApi: FilmWorldApi
    ): DetailRepository {
        return DetailRepository(
            detailDao, providerDao, cinemaWorldApi, filmWorldApi
        )
    }
}
