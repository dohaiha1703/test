package com.johnny.princestheatre.repository

import android.util.Log
import com.johnny.princestheatre.database.dao.DetailDao
import com.johnny.princestheatre.database.dao.ProviderDao
import com.johnny.princestheatre.database.entity.DBDetail
import com.johnny.princestheatre.database.entity.DBMediaProvider
import com.johnny.princestheatre.database.entity.ProviderName
import com.johnny.princestheatre.model.MediaDetail
import com.johnny.princestheatre.model.MediaDetailPrice
import com.johnny.princestheatre.network.api.CinemaWorldApi
import com.johnny.princestheatre.network.api.FilmWorldApi
import com.johnny.princestheatre.network.entity.TestMovieDetailResponse
import com.johnny.princestheatre.util.TAG
import retrofit2.Response

class DetailRepository(
    private val detailDao: DetailDao,
    private val providerDao: ProviderDao,
    private val cinemaWorldApi: CinemaWorldApi,
    private val filmWorldApi: FilmWorldApi
) {
    /*
        Fetch the latest details from the APIs. If the API is unavailable, try to find a cached
        version in th DB. The information displayed in the activity is only from the first provider.
        It would've been better to get this from a central 3rd party (like IMDb or TMDb etc.), but
        this information isn't available in the movie list APIs.
     */
    suspend fun fetchDetails(mediaId: String): MediaDetail? {
        val detailList: MutableList<MediaDetail> = mutableListOf()
        val providerList: List<DBMediaProvider> = providerDao.getProviders(mediaId)

        // Loop through providers with the movie available
        providerList.forEach { provider: DBMediaProvider ->
            val detail = when (provider.provider) {
                ProviderName.CinemaWorld -> fetchDetailFromCinemaWorldApi(provider.providerId, mediaId)
                ProviderName.FilmWorld -> fetchDetailFromFilmWorldApi(provider.providerId, mediaId)
            }

            if (detail != null)
                detailList.add(detail)
        }

        var detail: MediaDetail? = null

        // Use the returned details if the API call is successful, otherwise try to get a cached version
        if (detailList.isNotEmpty()) {
            // Save the information from the first successful API as the movie details.
            detail = detailList.first()
            // Really need to do these in the constructor
            detailDao.insert(DBDetail(
                mediaId, detail.title, detail.year, detail.rated, detail.released, detail.runtime,
                detail.genre, detail.director, detail.writer, detail.actors, detail.plot, detail.language,
                detail.country, detail.poster, detail.type, detail.production
            ))
        }
        else {
            val dbDetail = detailDao.getDetail(mediaId)
            if (dbDetail != null)
                detail = MediaDetail(
                    mediaId, dbDetail.title, dbDetail.year, dbDetail.rated, dbDetail.released, dbDetail.runtime,
                    dbDetail.genre, dbDetail.director, dbDetail.writer, dbDetail.actors, dbDetail.plot, dbDetail.language,
                    dbDetail.country, dbDetail.poster, dbDetail.type, dbDetail.production
                )
        }

        if (detail == null)
            return null

        // Get the latest available provider info, even if their API call failed
        val dbProviders: List<DBMediaProvider> = providerDao.getProviders(mediaId)
        detail.prices = dbProviders.map {
            MediaDetailPrice(it.provider, it.price)
        }

        return detail
    }

    suspend fun fetchDetailFromCinemaWorldApi(providerId: String, mediaId: String): MediaDetail? {
        val request = kotlin.runCatching {
            cinemaWorldApi.getMovieDetails(providerId)
        }
        request.onSuccess { response: Response<TestMovieDetailResponse> ->
            val body: TestMovieDetailResponse? = response.body()
            if (response.isSuccessful && body != null) {
                val dbMediaProvider = DBMediaProvider(mediaId, providerId, ProviderName.CinemaWorld)
                dbMediaProvider.price = body.price
                providerDao.insert(dbMediaProvider)
                return MediaDetail(
                    mediaId, body.title, body.year.toInt(), body.rated, body.released, body.runtime,
                    body.genre, body.director, body.writer, body.actors, body.plot, body.language,
                    body.country, body.poster, body.type, body.production
                )
            }
        }
        request.onFailure { e: Throwable ->
            Log.e(TAG, "Error fetching details from Cinema World API", e)
        }

        return null
    }

    private suspend fun fetchDetailFromFilmWorldApi(providerId: String, mediaId: String): MediaDetail? {
        val request = kotlin.runCatching {
            filmWorldApi.getMovieDetails(providerId)
        }
        request.onSuccess { response: Response<TestMovieDetailResponse> ->
            val body: TestMovieDetailResponse? = response.body()
            if (response.isSuccessful && body != null) {
                val dbMediaProvider = DBMediaProvider(mediaId, providerId, ProviderName.FilmWorld)
                dbMediaProvider.price = body.price
                providerDao.insert(dbMediaProvider)
                return MediaDetail(
                    mediaId, body.title, body.year.toInt(), body.rated, body.released, body.runtime,
                    body.genre, body.director, body.writer, body.actors, body.plot, body.language,
                    body.country, body.poster, body.type, body.production
                )
            }
        }
        request.onFailure { e: Throwable ->
            Log.e(TAG, "Error fetching details from Film World API", e)
        }

        return null
    }
}