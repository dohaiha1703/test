package com.johnny.princestheatre.repository

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.johnny.princestheatre.database.dao.DetailDao
import com.johnny.princestheatre.database.dao.MediaDao
import com.johnny.princestheatre.database.dao.ProviderDao
import com.johnny.princestheatre.database.entity.*
import com.johnny.princestheatre.model.Movie
import com.johnny.princestheatre.network.api.CinemaWorldApi
import com.johnny.princestheatre.network.api.FilmWorldApi
import com.johnny.princestheatre.network.entity.TestMovieResponse
import com.johnny.princestheatre.util.LAST_SYNC_TIME
import com.johnny.princestheatre.util.SHARED_PREF_LAST_SYNC
import com.johnny.princestheatre.util.SHARED_PREF_PRIVATE_MODE
import com.johnny.princestheatre.util.TAG
import org.threeten.bp.Instant
import org.threeten.bp.temporal.ChronoUnit
import retrofit2.Response
import java.util.*

class MovieRepository(
    applicationContext: Context,
    private val mediaDao: MediaDao,
    private val providerDao: ProviderDao,
    private val detailDao: DetailDao,
    private val cinemaWorldApi: CinemaWorldApi,
    private val filmWorldApi: FilmWorldApi
) {
    private val lastSyncSharedPref: SharedPreferences = applicationContext.getSharedPreferences(
        SHARED_PREF_LAST_SYNC, SHARED_PREF_PRIVATE_MODE
    )

    /*
        Fetching the list of movies from either the APIs or the database. If the movie list has
        been acquired in the last 6 hours, then use the cached version from the DB. This will
        minimise the use of the unstable APIs and speed up the population of the home grid.
     */

    suspend fun fetchMovieList(): List<Movie> {
        var movieList: List<Movie> = listOf()

        val lastSyncTime: Instant = Instant.ofEpochSecond(
            lastSyncSharedPref.getLong(LAST_SYNC_TIME, 0L)
        )

        if (lastSyncTime.isAfter(Instant.now().minus(6L, ChronoUnit.HOURS)))
            movieList = fetchMovieListFromDb()

        return if (movieList.isNotEmpty())
            movieList
        else
            fetchMovieListFromApi()
    }

    private suspend fun fetchMovieListFromDb(): List<Movie> {
        return mediaDao.selectAll().map {
            Movie(it.id, it.title, it.poster)
        }
    }

    suspend fun refreshMovieList(): List<Movie> {
        return fetchMovieListFromApi()
    }

    /*
        Fetching the list of available movies from each of the available APIs. If this was
        a real world situation, it has to be assumed that each of the sources will have different
        lists of movies (regional exclusivity, etc.) and also that the data in the returned JSON
        won't necessarily line up or play nicely together. Preferably it would've been much better
        if the returned list had a 3rd party id (like IMDb or TMDb), or to have gotten the list
        from a central API rather than building a list from multiple places.
    */
    private suspend fun fetchMovieListFromApi(): List<Movie> {
        val cinemaWorldMediaList: List<DBMedia>? = fetchCinemaWorldMovieList()
        val filmWorldMediaList: List<DBMedia>? = fetchFilmWorldMovieList()

        // Let the DB build the unique list without duplicate movies
        val dbMediaList: List<DBMedia> = mediaDao.selectAll()
        val movieList: MutableList<Movie> = mutableListOf()
        val movieMap: MutableMap<String, String> = mutableMapOf()

        // Add each unique movie to the return list, plus build a hashmap to make lining up the movie provider's movie id to the movie entry more efficient
        dbMediaList.forEach { media: DBMedia ->
            movieList.add(Movie(media.id, media.title, media.poster))
            movieMap[media.title] = media.id
        }

        // Make provider entries for each movie, so when getting details the app knows what ID to send to the API
        val providerList: MutableList<DBMediaProvider> = mutableListOf()

        if (!cinemaWorldMediaList.isNullOrEmpty()) {
            cinemaWorldMediaList.forEach { media: DBMedia ->
                if (movieMap.containsKey(media.title)) {
                    providerList.add(
                        DBMediaProvider(movieMap[media.title] ?: "", media.providerId, ProviderName.CinemaWorld)
                    )
                }
            }
        }

        if (!filmWorldMediaList.isNullOrEmpty()) {
            filmWorldMediaList.forEach { media: DBMedia ->
                if (movieMap.containsKey(media.title)) {
                    providerList.add(
                        DBMediaProvider(movieMap[media.title] ?: "", media.providerId, ProviderName.FilmWorld)
                    )
                }
            }
        }

        // Update the last sync time to now and save to the shared preferences
        if (!providerList.isNullOrEmpty()) {
            providerDao.insert(*providerList.toTypedArray())
            val editor = lastSyncSharedPref.edit()
            editor.putLong(LAST_SYNC_TIME, Instant.now().epochSecond)
            editor.apply()
        }

        //Delete movies not available from any provider
        mediaDao.deleteOrphans()
        detailDao.deleteOrphans()

        return movieList
    }

    /*
        Call the movie provider's API to get the list of movies available from that API. I thought
        it was important to treat the two test movie providers as separate and different, even
        though their data lines up quite nicely. In the real world, providers like iTunes and
        Amazon would format their content in completely different ways.
     */
    suspend fun fetchCinemaWorldMovieList(): List<DBMedia>? {
        val request = kotlin.runCatching {
            cinemaWorldApi.getAllMovies()
        }
        request.onSuccess { response: Response<TestMovieResponse> ->
            val body: TestMovieResponse? = response.body()
            if (response.isSuccessful && body != null) {
                // if the data is successfully returned, delete the existing provider data
                providerDao.deleteProvider(ProviderName.CinemaWorld)
                val dbMediaList: MutableList<DBMedia> = mutableListOf()
                // map the api data to the db format. Would've been best to do this in a constructor in the db entity
                body.movies.forEach {
                    val media = DBMedia(it.title, it.poster, when(it.type.toLowerCase(Locale.getDefault())){
                        "movie" -> MediaType.Movie
                        else -> MediaType.Unknown
                    })
                    // this value get's ignored by room, but is needed in the calling function
                    media.providerId = it.id
                    dbMediaList.add(media)
                }

                mediaDao.insert(*dbMediaList.toTypedArray())
                return dbMediaList
            }
        }
        request.onFailure { e: Throwable ->
            Log.e(TAG, "Error fetching movies from Cinema World", e)
        }

        return null
    }

    private suspend fun fetchFilmWorldMovieList(): List<DBMedia>? {
        val request = kotlin.runCatching {
            filmWorldApi.getAllMovies()
        }
        request.onSuccess { response: Response<TestMovieResponse> ->
            val body: TestMovieResponse? = response.body()
            if (response.isSuccessful && body != null) {
                providerDao.deleteProvider(ProviderName.FilmWorld)
                val dbMediaList: MutableList<DBMedia> = mutableListOf()
                body.movies.forEach {
                    val media = DBMedia(it.title, it.poster, when(it.type.toLowerCase()){
                        "movie" -> MediaType.Movie
                        else -> MediaType.Unknown
                    })
                    media.providerId = it.id
                    dbMediaList.add(media)
                }

                mediaDao.insert(*dbMediaList.toTypedArray())
                return dbMediaList
            }
        }
        request.onFailure { e: Throwable ->
            Log.e(TAG, "Error fetching movies from Film World", e)
        }

        return null
    }
}
