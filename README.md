# README #

Prince's Theatre Android app for Lexicon coding challenge

### Build Instructions ###

The project should build as normal from within Android Studio.

### Libraries and Architecture ###

I tried to use fairly a fairly standard set of libraries for this app:

* Hilt for the dependancy injection
* Retrofit for API calls
* Room for the SQLite DB
* Moshi for the JSON de/serialisation
* Coroutines for the async calls
* Glide for remote images
* ThreeTenABP to get Java 8 dates on the older targeted Android SDK

Overall I'm pretty familiar with these libraries, other than hilt. Previously I've used Kodein for DI, but I thought this was a good opportunity to learn something new!
Architecture is also pretty standard, Activity -> ViewModel -> Repository -> DB/API.

### Assumptions Made ###

My general assumption for how to use the provided APIs was to treat them as 2 unrelated and potentially incompatable data sets. I think this reflects a real world problem better.
Take for example 2 real online movie providers - Amazon and iTunes. They will have different ways of storing the data, different ID formats, and a completely different catalog of movies. I didn't want to assume that the data sets contained the same catalog of movies.
I understand that in the APIs given that the catalogs were the same and the IDs were fairly aligned, but I wanted to pretend that I was dealing with real world providers, keeping with the spirit of the customer brief.

### Trade Offs ###

Because I was getting 2 potentially different catalogs of movies, the merging of the 2 lists is fairly ineffient in the MovieRepository. Preferably getting a list of movies from a central location, such as a 3rd party like IMDb or TMDb, would be a much more effient solution. Unfortunely the provided APIs lacked and global identifier which would make this fairly difficult. Also the movie list API lacked the release year which would make different movies with the same name combine into the same entry. Overall, I feel like the APIs provided are quite unsuitable to make a real world workable solution accurate and effient.
Also, the lack of a global identifier made it so that I had to save the movie details from one of the providers. In a real world situation I think this is quite unideal as well, as different providers could have differing levels of quality or content.

### Data Design Decision ###

I decided to have a list of unique movies that have a one-to-many relationship with a list of providers, and a one-to-one relationship with a list of details. The provider list contained the name of the provider, the id the provider assigned to the movie, the price which will be empty unti the detail API is called, and a foreign key that links it back to the movie. I feel like this was the most efficient and expandable way to store the data, as it will be easy to add new providers in the future, and the query to populate the recyclerview on the home screen only has to go through one set of movies with a `select *`.

### Final Thoughts ###

Overall I found this a pretty fun exercise. On the surface it seems like quite an easy and straight forward problem, but once I dived deeper into the proviced data I found that there were some tough decisions to make.
If I had more time, perhaps it would've been fun to not use the test APIs and to make it an actual real world working app calling real APIs. Maybe it could be possible to get the provider id and deeplink into their app (like get the netflix id then open netflix to that movie). Another cool netflix related idea could be to query the global netflix catalog, then automatically start a VPN and play it if it's not available in your region. It was fun to let my imagination run wild on this problem!

Also what was quite cool for me was that it was my first time using a mock server to unit test the API calls. I use every opportunity to learn something new in any project I do.